// gọi id
function getELE(id){
  return document.getElementById(id);
}
// sidebar
$('.sub-btn').click(function(){
  $(this).next('.sub-menu').slideToggle();
  $(this).find('.arrow').toggleClass('rotate');
});

let sidebar = document.querySelector(".sidebar");
let sbBtn = document.querySelector(".fa-bars");
sbBtn.addEventListener("click", function(){
  sidebar.classList.toggle("active");
});

$('.editbtn').click(function(){
  $('#myModal').modal('show');
    let id = $(this).attr("id");
    $.ajax({
      url:"../config/viewDetail.php",
      method:"POST",
      data:{id:id},
      dataType:"json",
      success:function(data){
        let ava = data.avatar;
        $('#user-avatar').attr('src','../images/'+ava);
        $('#name').val(data.name);
        $('#username').val(data.username);
        $('#department').val(data.department);
        $('#role').val(data.role);
        $('#day-leave').val(data.day_leave);
      }
    })
});
