  <section class="sidebar">
    <div class="logo-sidebar">
        <i class="fab fa-digital-ocean"></i>
    </div>
    <ul class="menu">
      <div class="item admin">
        <a href="trang-chu.php">
          <i class="material-icons title">dashboard</i>
          <span>Trang chủ</span>
        </a>
      </div>
      <div class="item admin">
        <a href="yeu-cau-nghi-phep.php">
          <i class="fa fa-check"></i>
          <span>Duyệt yêu cầu</span>
        </a>
      </div>
      <div class="item admin">
          <a class="sub-btn">
            <i class="fa fa-users title"></i>
            <span>Nhân viên</span>
             <i class="fas fa-angle-right arrow"></i>
          </a>
          <div class="sub-menu">
           <a href="danh-sach-nhan-vien.php" class="sub-item">Quản lý nhân viên</a>
           <a href="them-nhan-vien.php" class="sub-item">Thêm nhân viên</a>
         </div>
      </div>
      <div class="item admin">
          <a class="sub-btn">
            <i class="fa fa-object-group title"></i>
            <span>Phòng ban</span>
            <i class="fas fa-angle-right arrow"></i>
          </a>
          <div class="sub-menu">
            <a class="sub-item" href="danh-sach-phong-ban.php">Quản lý phòng ban</a>
            <a class="sub-item" href="them-phong-ban.php">Thêm phòng ban</a>
          </div>
      </div>
      <!-- Chỗ này của nhân viên-->
      <div class="item staff-task">
          <a href="trang-chu.php">
            <i class="fa fa-tasks title"></i>
            <span>Nhiệm vụ</span>
          </a>
      </div>
      <!-- Chỗ nãy của trưởng phòng -->
      <div class="item leader">
          <a class="sub-btn">
          <i class="fa fa-tasks title"></i>
            <span>Nhiệm vụ</span>
             <i class="fas fa-angle-right arrow"></i>
          </a>
          <div class="sub-menu">
           <a href="quan-ly-nhiem-vu.php" class="sub-item">Quản lý nhiệm vụ</a>
           <a href="giao-nhiem-vu.php" class="sub-item">Giao nhiệm vụ</a>
         </div>
      </div>
      <!-- của nhân viên và trưởng phòng -->
      <div class="item staff">
          <a href="nghi-phep.php">
            <i class="fa fa-check-double title"></i>
            <span>Nghỉ phép</span>
          </a>
      </div>
</section>

