<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');
?>
    <title>Yêu cầu nghỉ phép</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    <section class="home-section">
        <?php require_once('./headbar.php') ?>
        <div class="dashboard">
            <div class="welcome-staff row">
				<div class="welcome-title col-md-6">
					<h2>Yêu cầu nghỉ phép</h2>
				</div>
			</div>
            <table class="table table-bordered task-table">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ và tên</th>
                        <th>Tên người dùng</th>
                        <th>Chức vụ</th>
                        <th>Thao tác</th>
                        <th>Trạng thái</th>
                    </tr>
                </thead>
                <tbody id="department-staff">
                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>nguyenvana</td>
                        <td>Nhân viên</td>
                        <td><button class="showLeaveInfo"  data-toggle="modal" data-target="#leave-request">Xem chi tiết</button></td>
                        <td>Chưa duyệt</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
    <div class="modal fade" id="leave-request">
		<div class="modal-dialog">
			<div class="modal-content py-3">
				<header class="head-form">
					<h3 id="header-title" class="px-3">Yêu cầu nghỉ phép</h3>
				</header>
				<!-- Modal body -->
				<div class="modal-body">
					<form role="form" id="show-Info-Department" class="mx-3">
                        <div class="row">
							<div class="form-group">
								<label class="control-label" for="leave-name">Họ và tên:</label>
								<input type="text" class="form-control" name="leave-name" id="leave-name" value = "">
                            </div>
						</div>
						<div class="row">
							<div class="form-group">
									<label class="control-label" for="leave-num">Số ngày nghỉ:</label>
									<input type="number" class="form-control" name="leave-num" id="leave-num" value = "">
                            </div>
						</div>
                        <label class="control-label" for="leave-rea">Lý do:</label>
                        <div class="row">
                            <div class="form-group">
                                <textarea name="leave-rea" id="leave-rea" class="form-control" cols="40" rows="4" placeholder=""></textarea>
                            </div>
                        </div>
						<div class="row">
                            <div class="custom-file">
                                <input type="file" class=" form-control custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Tệp đính kềm</label>
                            </div>
                        </div>
					</form>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer" id="modal-footer">
					<button id="btnCheckLeave" onclick="" type="button">Duyệt</button>
                    <button id="btnUncheckLeave" onclick="" type="button">Không duyệt</button>
					<button id="btnClose" type="button" data-dismiss="modal" onclick="resetForm()">Đóng</button>
				</div>
			</div>
		</div>
</div>
<?php require_once('../Layouts/footer.php');
}else{
    header('location:../index.php');
  } ?>