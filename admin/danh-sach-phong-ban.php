<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');;
?>
    <title>Danh sách phòng ban</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
	<section class="home-section">
		<?php require_once('./headbar.php') ?>
		<section class="dashboard">
			<div class="welcome-staff row">
				<div class="welcome-title col-md-6">
					<h2>Danh sách phòng ban</h2>
				</div>
			</div>
			<div class="main-body py-4">
				<div class="row mb-3">
					<div class="col">
						<div class="input-group w-50">
							<input type="text" class="form-control" placeholder="Tìm kiếm" id="searchName">
							<div class="input-group-prepend">
								<span class="input-group-text" id="btnSearchUser"><i class="fa fa-search"></i></span>
							</div>
						</div>
					</div>
				</div>
				<!-- Đưa dữ liệu lên phần này -->
				<div class="show-Department py-1 m-auto">
                    <table class="table table-bordered depTable">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên phòng ban</th>
                                <th>Mô tả</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="depBody">
                            <!-- data -->
							<?php require_once('../config/department.php')?>
                        </tbody>
                    </table>
				</div>			
			</div>
		</section>
	</section>
	<div class="modal fade" id="myModalDepartment">
		<div class="modal-dialog">
			<div class="modal-content">
				<header class="head-form px-3">
					<h3 id="header-title">Thông tin phòng ban</h3>
				</header>
				<!-- Modal body -->
				<div class="modal-body">
					<form role="form" id="show-Info-Department">
						<div class="row">
							<div class="form-group px-2 lenght">
									<label class="control-label" for="department-name">Tên phòng ban:</label>
									<input type="text" class="form-control" name="department-name" id="department-name" value = "" disabled>
							</div>
							<div class="form-group px-2">
								<label class="control-label" for="user-name">Số phòng:</label>
								<input type="text" class="form-control" name="department-room" id="department-room" value = "" disabled>
							</div>
						</div>
						<div class="form-group">
							<textarea name="department-description" id="department-description" cols="50" rows="8" placeholder=" Mô tả" disabled></textarea>
						</div>
						<div class="row">
							<div class="form-group px-2">
								<label class="control-label" for="user-name">Trưởng phòng:</label>
								<input type="text" class="form-control" name="department-leader" id="department-leader" value = "" disabled>
							</div>
						</div>
					</form>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer" id="modal-footer">
					<button id="btnUpdateStaff" onclick="layThongTinNV()" type="button">Chỉnh sửa</button>
					<button id="btnClose" type="button" data-dismiss="modal">Đóng</button>
				</div>
			</div>
		</div>
	</div>

<?php require_once('../Layouts/footer.php');
}else{
	header('location:../index.php');
  }
   ?>