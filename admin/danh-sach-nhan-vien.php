<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');;
?>
    <title>Danh sách nhân viên</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
	<section class="home-section">
		<?php require_once('./headbar.php') ?>
		<section class="dashboard">
			<div class="welcome-staff row">
				<div class="welcome-title col-md-6">
					<h2>Danh sách nhân viên</h2>
				</div>
			</div>
			<div class="main-body py-4">
				<div class="row mb-3">
					<div class="col">
						<div class="input-group w-50">
							<input type="text" class="form-control" placeholder="Tìm kiếm" id="searchName">
							<div class="input-group-prepend">
								<span class="input-group-text" id="btnSearchUser"><i class="fa fa-search"></i></span>
							</div>
						</div>
					</div>
				</div>
				<!-- Đưa dữ liệu lên phần này -->
				<div class="show-staff py-1 m-auto">
					<div class="row" id="show-staff">
						<!-- thêm data ở đây -->
						<?php 
							$sql = "SELECT * FROM employee";
							$result = $connect->query($sql);
							while($row = $result->fetch_assoc()){
								echo "<div class='staff-card col-lg-3 col-md-6'>
								<div class='item'>
								  <div class='item-img'>
									<img src='../images/".$row['avatar']."' alt=''>
								  </div>
								  <div class='item-info'>
									<h5>".$row['name']."</h5>
									<p>Phòng ban: ".$row['department']."</p>
									<p>Chức vụ: ".$row['role']."</p>
									<a class='editbtn' id=".$row['id']." >Xem chi tiết</a>
								  </div>
								</div>
							  </div>";
							}
						?>
					</div>
				</div>			
			</div>
		</section>
	</section>
	<div class="modal fade" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<header class="head-form px-3">
					<h3 id="header-title">Thông tin nhân viên</h3>
				</header>
				<!-- Modal body -->
				<div class="modal-body">
					<form role="form" id="show-Info-Staff">
						<div class="row col-6">
							<div class="form-group">
									<img name="avatar" id="user-avatar" src="" alt="user-avatar">
							</div>
						</div>
						<div class="row">
							<div class="form-group px-2">
									<label class="control-label" for="name">Họ và tên:</label>
									<input type="text" class="form-control update" name="name" id="name" value = "" disabled>
							</div>
						</div>
						<div class="row">
							<div class="form-group px-2">
								<label class="control-label" for="name">Tên người dùng:</label>
								<input type="text" class="form-control update" name="username" id="username" value = "" disabled>
							</div>
						</div>
						<div class="row">
							<div class="form-group px-2">
								<label class="control-label" for="department">Phòng ban:</label>
								<input type="text" class="form-control" name="department" id="department" value = "" disabled>
							</div>
							<div class="form-group px-2">
								<label class="control-label" for="user-name">Chức vụ:</label>
								<input type="text" class="form-control" name="role" id="role" disabled>
							</div>
						</div>
						<div class="row">
							<div class="form-group px-2">
								<label class="control-label" for="day-leave">Số ngày đã xin nghỉ:</label>
								<input type="number" class="form-control" name="day-leave" id="day-leave" disabled>
							</div>
						</div>
					</form>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer" id="modal-footer">
					<button id="btnSaveStaff" type="button">Reset mật khẩu</button>
					<button id="btnClose" type="button" data-dismiss="modal">Đóng</button>
				</div>
			</div>
		</div>
	</div>

<?php require_once('../Layouts/footer.php');
}else{
	header('location:../index.php');
  } ?>