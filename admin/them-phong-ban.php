<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');
?>
    <title>Thêm phòng ban</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    <section class="home-section">
        <?php require_once('./headbar.php') ?>
        <div class="dashboard">
            <div class="welcome-staff row">
				<div class="welcome-title col-md-6">
					<h2>Thêm phòng ban</h2>
				</div>
			</div>
            <br>
            <div class="show-form m-auto">
                <form class="add-form m-auto py-5" action="../config/process.php" method="POST">
                    <div class="form-group">
                        <label class="control-label" for="departmentName">Tên phòng ban:</label>
                        <input type="text" class="form-control" name="departmentName" id="departmentName" value = "">
                    </div>
                    <div class="form-group">
                        <textarea name="deDes" class="form-control" id="deDes" cols="50" rows="8" placeholder=" Mô tả"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="depNumber">Số phòng</label>
                        <input type="text" class="form-control" name="depNumber" id="depNumber" value = "">
                    </div>
                    <button type="submit" id="btnThemNV"><i class="fa fa-plus"></i> Thêm phòng ban</button>
                </form>
            </div>
        </div>
    </section>
<?php require_once('../Layouts/footer.php'); 
}else{
    header('location:../index.php');
  }
?>