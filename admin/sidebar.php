  <section class="sidebar">
    <div class="logo-sidebar">
        <i class="fab fa-digital-ocean"></i>
    </div>
    <ul class="menu">
      <div class="item admin">
        <a href="trang-chu.php">
          <i class="material-icons title">dashboard</i>
          <span>Trang chủ</span>
        </a>
      </div>
      <div class="item admin">
        <a href="yeu-cau-nghi-phep.php">
          <i class="fa fa-check"></i>
          <span>Duyệt yêu cầu</span>
        </a>
      </div>
      <div class="item admin">
          <a class="sub-btn">
            <i class="fa fa-users title"></i>
            <span>Nhân viên</span>
             <i class="fas fa-angle-right arrow"></i>
          </a>
          <div class="sub-menu">
           <a href="danh-sach-nhan-vien.php" class="sub-item">Quản lý nhân viên</a>
           <a href="them-nhan-vien.php" class="sub-item">Thêm nhân viên</a>
         </div>
      </div>
      <div class="item admin">
          <a class="sub-btn">
            <i class="fa fa-object-group title"></i>
            <span>Phòng ban</span>
            <i class="fas fa-angle-right arrow"></i>
          </a>
          <div class="sub-menu">
            <a class="sub-item" href="danh-sach-phong-ban.php">Quản lý phòng ban</a>
            <a class="sub-item" href="them-phong-ban.php">Thêm phòng ban</a>
          </div>
      </div>
</section>

