<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');
?>
    <title>Trang chủ</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    
<section class="home-section">
    <?php require_once('./headbar.php') ?>
    <div class="dashboard">
        <div class="welcome">
                <h1>Xin chào <span class="user-name"> tên người dùng!</span></h1>
        </div>
        <div class="row">
            <div class="db-card col-lg-3 col-sm-6 col-12">
                <div class="db-item">
                    <div class="db-box1">
                        <div class="db-box-icon">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <div class="db-box2">
                        <div class="db-box-content">
                                <!-- lấy từ database xuống -->
                                <h3>Số nhân viên</h3>
                                <p>Nhân viên</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="db-card col-lg-3 col-sm-6 col-12">
                <div class="db-item">
                    <div class="db-box1">
                        <div class="db-box-icon">
                        <i class="material-icons">work</i>
                        </div>
                    </div>
                    <div class="db-box2">
                        <div class="db-box-content">
                                <!-- lấy từ database xuống -->
                                <h3>Số ban</h3>
                                <p>Phòng ban</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="db-card col-lg-3 col-sm-6 col-12">
                <div class="db-item">
                    <div class="db-box1">
                        <div class="db-box-icon">
                        <i class="fa fa-tasks"></i>
                        </div>
                    </div>
                    <div class="db-box2">
                        <div class="db-box-content">
                                <!-- lấy từ database xuống -->
                                <h3>Số task</h3>
                                <p>Công việc</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="db-card col-lg-3 col-sm-6 col-12">
                <div class="db-item">
                    <div class="db-box1">
                        <div class="db-box-icon">
                            <i class="material-icons">done_outline</i>
                        </div>
                    </div>
                    <div class="db-box2">
                        <div class="db-box-content">
                                <!-- lấy từ database xuống -->
                                <h3>Số</h3>
                                <p>Nghỉ phép</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="newInfo">
            <div class="newStaff">
                <!-- chỗ này in khoảng 3nv mới từ database -->
                <h3>Nhân viên mới</h3>
                <table class="table table-bordered table-hover myTable">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Họ và Tên</th>
                            <th>Tên người dùng</th>
                            <th>Phòng ban</th>
                            <th>Chức vụ</th>
                        </tr>
                    </thead>
                    <tbody id="staffBody">
                        <!-- data -->
                        <tr>
                            <td>1</td>
                            <td>Ngo Diem Hue</td>
                            <td>ngodiemhue</td>
                            <td>Marketing</td>
                            <td>Nhân viên</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="newGrp">
                <!-- chỗ này in khoảng 3 phòng ban mới từ database -->
                <h3>Phòng ban mới</h3>
                <table class="table table-bordered table-hover myTable">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên phòng ban</th>
                            <th>Mô tả</th>
                            <th>Số phòng</th>
                        </tr>
                    </thead>
                    <tbody id="grpBody">
                        <!-- data -->
                        <tr>
                            <td>1</td>
                            <td>Marketing</td>
                            <td>Demo thử hoy</td>
                            <td>A003</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<?php require_once('../Layouts/footer.php'); 
    }else{
        header('location:../index.php');
      }
?>