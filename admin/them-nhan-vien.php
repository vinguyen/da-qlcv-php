<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');
        $error = '';
        $name = '';
        $user = '';
        $dep = '';

        if (isset($_POST['add-name']) && isset($_POST['add-user-name']) && isset($_POST['add-phong-ban'])){
            $name = $_POST['add-name'];
            $user = $_POST['add-user-name'];
            $dep = $_POST['add-phong-ban'];
            if (empty($name)) {
                $error = "<div class='alert alert-info my-3'>Hãy nhập họ tên</div>";
            }
            else if (empty($user)) {
                $error = "<div class='alert alert-info my-3'>Hãy nhập tên tài khoản</div>";
            }
            else if (empty($dep)) {
                $error = "<div class='alert alert-info my-3'>Hãy nhập tên phòng ban</div>";
            }
            else {
                if(isset($_POST['themNV'])){
                    if($_POST){
                        $pass = password_hash($user,PASSWORD_BCRYPT);
                        $connect->query("INSERT INTO `employee`(`id`,`name`, `username`, `password`, `department`, `role`, `avatar`, `day_leave`) VALUES ('','$name','$user','$pass','$dep','Nhân viên',0,'NO-IMAGE-AVAILABLE.jpg')") or die($connect->error);
                    }
                }
                $error = "<div class='alert alert-info my-3'>Thêm thành công!</div>";
            }
        }
    ?>
    <title>Thêm nhân viên</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    <section class="home-section">
        <?php require_once('./headbar.php') ?>
        <div class="dashboard">
            <div class="welcome-staff row">
				<div class="welcome-title col-md-6">
					<h2>Thêm nhân viên</h2>
				</div>
			</div>
            <br>
            <div class="show-form m-auto">
                <form class="add-form m-auto py-5" action="" method="POST">
                    <div class="form-group">
                        <label class="control-label" for="name">Họ và tên:</label>
                        <input type="text" class="form-control" name="add-name" id="add-name" value = "">
                        <div class="thongbao" id="txtName"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="user-name">Tên người dùng:</label>
                        <input type="text" class="form-control" name="add-user-name" id="add-user-name" value = "">
                        <div class="thongbao" id="txtUserName"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="name">Phòng ban</label>
                        <input type="text" class="form-control" name="add-phong-ban" id="add-phong-ban" value = "">
                        <div class="thongbao" id="txtDepName"></div>
                    </div>
                    <button type="submit" id="btnThemNV" name="themNV"><i class="fa fa-plus"></i> Thêm nhân viên</button>
                    <?php echo $error?>
                </form>
            </div>
        </div>
    </section>
<?php require_once('../Layouts/footer.php');
}else{
    header('location:../index.php');
  } ?>