<?php
session_start();
include('../config/db.php');

if(isset($_SESSION['id'])){
    $user_id = $_SESSION['id'];
    require_once('../Layouts/header.php');
    date_default_timezone_set('Asia/Ho_Chi_Minh');
?>


<?php
    function upload_files($task_id){
        $uploadTo = "../uploads/";
        $allowFileExt = array('jpg','png','jpeg','gif','pdf','doc','csv','zip','txt');
        $fileName = array_filter($_FILES['file_name']['name']);
        $fileTempName=$_FILES["file_name"]["tmp_name"];
        $tableName= trim('files');

        if (empty($fileName)) {
            $error="Please Select files..";
            return $error;
        }
        else if(empty($tableName)){
            $error="You must declare table name";
            return $error;
        }
        else{
            $error=$storeFilesBasename='';

            foreach($fileName as $index=>$file){
                $fileBasename = basename($fileName[$index]);
                var_dump($fileBasename);
                $filePath = $uploadTo.md5($index.$fileBasename.date_timestamp_get(date_create())).'-'.$fileBasename;
                $fileUrl = '/uploads/'.md5($index.$fileBasename.date_timestamp_get(date_create())).'-'.$fileBasename;
                $fileExt = pathinfo($filePath, PATHINFO_EXTENSION);
                if(in_array($fileExt, $allowFileExt)){
                    if(move_uploaded_file($fileTempName[$index],$filePath)){
                        store_files($fileBasename, $fileUrl, $task_id);
                    }
                    else{
                        $error = 'File Not uploaded ! try again';
                    }
                }
                else{
                    $error .= $_FILES['file_name']['name'][$index].' - file extensions not allowed<br> ';
                }
            }

        }

        return $error;
    }

    function store_files($fileBasename, $filePath, $task_id){
        $connect = mysqli_connect('localhost', 'root', '', 'doan');
        if (!empty($fileBasename)) {
            $sqlFile = "INSERT INTO files SET 
                file_name = '$fileBasename',
                url = '$filePath',
                parent_id = '$task_id',
                type = 1
            ";
            $exec = mysqli_query($connect, $sqlFile);
            if($exec){
                echo "files are uploaded successfully";
            }else{
                echo  "Error: " .  $sqlFile . "<br>";
            }
        }
    }
?>

<?php
if (isset($_POST['submit'])) {

    $title = $_POST['title'];
    $description = $_POST['description'];
    $assignee_id = $_POST['assignee_id'];
    $expired_at = $_POST['expired_at'];
    $created_at = date('Y-m-d H:i:s');
    $updated_at = date('Y-m-d H:i:s');
    $created_by = $_SESSION['id'];

    if (!empty($title) || !empty($assignee_id)) {
        $sql1 = "INSERT INTO tasks SET 
                title = '$title',
                description = '$description',
                assignee_id = '$assignee_id',
                expired_at = '$expired_at',
                created_at = '$created_at',
                updated_at = '$updated_at',
                status = '1',
                created_by = '$created_by'
            ";

        $res2 = mysqli_query($connect, $sql1);

        if ($res2 == true) {
            echo upload_files(mysqli_insert_id($connect));
            echo "<script type='text/javascript'>alert('Thêm công việc thành công');</script>";
            header("Refresh:0");
        }
        else {
            echo "<script type='text/javascript'>alert('Thêm công việc thất bại');</script>";
            header("Refresh:0");
        }
    }
    else {
        echo "<script type='text/javascript'>alert('Thêm công việc thất bại');</script>";
        header("Refresh:0");
    }

}
?>

<title>Danh sách công việc</title>
</head>
<body>
<?php require_once('./sidebar.php') ?>

<section class="home-section">
    <?php require_once('./headbar.php') ?>
    <div class="dashboard">
        <div class="welcome">
            <h1>Quản lý công việc</h1>
        </div>
        <div>
            <button class="creatTaskInfo"  data-toggle="modal" data-target="#myAddTask">Thêm nhiệm vụ</button>
        </div>
        <div class="task-board py-2">
            <table class="table table-bordered task-table">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Tiêu đề</th>
                    <th>Người nhận</th>
                    <th>Thời gian tạo</th>
                    <th>Dealine</th>
                    <th>Trạng thái</th>
                </tr>
                </thead>
                <tbody id="task-list-leader success">
                <?php
                $sql = "SELECT tasks.id, tasks.title, tasks.status, employee.name as assignee_name, 
                        tasks.expired_at,tasks.created_at
                        FROM tasks
                        INNER JOIN employee ON tasks.assignee_id = employee.id
                        WHERE created_by=".$user_id."
                        ORDER BY tasks.created_at DESC
                ";

                $result = $connect->query($sql);
                $index = 0;
                while($row = $result->fetch_assoc()){
                    $index++;
                    ?>
                        <tr>
                            <td><?=$index?></td>
                            <td>
                                <a href="chi-tiet-cong-viec.php?task_id=<?=$row['id']?>"><?= $row['title'] ?></a>
                            </td>
                            <td> <?= $row['assignee_name'] ?> </td>
                            <td><?= date_format(date_create($row['created_at']),"H:i:s d-m-Y") ?></td>
                            <td><?= date_format(date_create($row['expired_at']),"H:i:s d-m-Y") ?></td>
                            <td> <?php
                                switch ($row['status']) {
                                    case 1:
                                       echo 'New';
                                        break;
                                    case 2:
                                        echo 'In progress';
                                        break;
                                    case 3:
                                        echo 'Canceled';
                                        break;
                                    case 4:
                                        echo 'Waiting';
                                        break;
                                    case 5:
                                        echo 'Rejected';
                                        break;
                                    case 6:
                                        echo 'Completed';
                                        break;
                                }

                                ?> </td>
                        </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
</body>

<div class="modal fade" id="myAddTask">
    <div class="modal-dialog">
        <div class="modal-content py-3">
            <header class="head-form">
                <h3 id="header-title" class="px-3">Tạo nhiệm vụ</h3>
            </header>
            <!-- Modal body -->
            <div class="modal-body">
                <form
                    role="form"
                    method="POST" action=""
                    id="show-Info-Department" class="mx-3"
                    enctype="multipart/form-data"
                >
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label" for="creat-task">Tiêu đề:</label>
                            <input type="text" class="form-control" name="title" id="creat-task" value = "">
                        </div>
                    </div>
                    <label class="control-label" for="task-description">Mô tả:</label>
                    <div class="row">
                        <div class="form-group">
                            <textarea
                                name="description"
                                class="form-control"
                                id="task-description" cols="40" rows="4"
                                placeholder=""></textarea>
                        </div>
                    </div>
                    <label class="control-label" for="task-description">Người thực hiện:</label>
                    <div class="row">
                        <div class="form-group">
                            <select
                                name="assignee_id"
                                class="form-select form-control"
                                aria-label="Default select example"
                            >
                                <option selected>Người thực hiện</option>
                                <?php
                                $sql = "SELECT * FROM employee WHERE department_id=".$_SESSION['department_id']." AND role='Nhân viên'";
                                $result = $connect->query($sql);
                                while($row = $result->fetch_assoc()){
                                    ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                    <?php
                                }
                                ?>

                                <?php
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label" for="date-task">Thời gian:</label>
                            <input
                                type="datetime-local"
                                class="form-control"
                                name="expired_at"
                                min="<?=date('Y-m-d H:i:s')?>"
                                id="expired_at"
                                value = "">
                        </div>
                    </div>
                    <div class="row">
                        <div class="custom-file">
                            <input type="file"
                                   class=" form-control custom-file-input"
                                   name="file_name[]" id="files"
                                   multiple>
                            <label class="custom-file-label" for="customFile">Tệp đính kềm</label>
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footer">
                        <input type="submit" name="submit" id="btnGiveTask" value="Giao nhiệm vụ" />
                        <button id="btnClose" type="reset" data-dismiss="modal">Đóng</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
        </div>
    </div>
</div>

    <?php require_once('../Layouts/footer.php');
}else{
    header('location:../index.php');
} ?>
