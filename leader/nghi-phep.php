<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');
?>
    <title>Nghỉ phép</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    
<section class="home-section">
    <?php require_once('./headbar.php') ?>
    <div class="dashboard">
        <div class="row">
                <div class="db-card col-md-4 col-12">
                    <div class="db-item">
                        <div class="db-box1">
                            <div class="db-box-icon">
                                <i class="fa fa-plus"></i>
                            </div>
                        </div>
                        <div class="db-box2">
                            <div class="db-box-content">
                                <p>Tổng số</p>
                                <h3>Số</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="db-card col-md-4 col-12">
                    <div class="db-item">
                        <div class="db-box1">
                            <div class="db-box-icon">
                            <i class="fa fa-calendar-check"></i>
                            </div>
                        </div>
                        <div class="db-box2">
                            <div class="db-box-content">
                                    <p>Đã nghỉ</p>
                                    <h3>Số</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="db-card col-md-4 col-12">
                    <div class="db-item">
                        <div class="db-box1">
                            <div class="db-box-icon">
                                <i class="material-icons">my_library_books</i>
                            </div>
                        </div>
                        <div class="db-box2">
                            <div class="db-box-content">
                                <p>Còn lại</p>
                                    <h3>Số</h3>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="leave-body">
            <button id="add-leave" data-toggle="modal" data-target="#myLeaveForm"><i class="fa fa-plus"></i> Thêm yêu cầu</button>
            <h3 class="py-3">Danh sách các yêu cầu từng tạo</h3>
            <table class="table table-bordered leave-list">
                <tr>
                    <th>STT</th>
                    <th>Số ngày nghỉ</th>
                    <th>Lý do</th>
                    <th>Trạng thái</th>
                </tr>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="myLeaveForm">
		<div class="modal-dialog">
			<div class="modal-content py-3">
				<header class="head-form">
					<h3 id="header-title" class="px-3">Yêu cầu nghỉ phép</h3>
				</header>
				<!-- Modal body -->
				<div class="modal-body">
					<form role="form" id="show-Info-Department" class="mx-3">
						<div class="row">
							<div class="form-group">
									<label class="control-label" for="leave-number">Số ngày nghỉ:</label>
									<input type="number" class="form-control" name="leave-number" id="leave-number" value = "">
                            </div>
						</div>
                        <label class="control-label" for="leave-reason">Lý do:</label>
                        <div class="row">
                            <div class="form-group">
                                <textarea name="leave-reason" id="leave-reason" class="form-control" cols="40" rows="4" placeholder=""></textarea>
                            </div>
                        </div>
						<div class="row">
                            <div class="custom-file">
                                <input type="file" class=" form-control custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Tệp đính kềm</label>
                            </div>
                        </div>
					</form>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer" id="modal-footer">
					<button id="btnSendLeave" onclick="sendLeaveRequest()" type="button">Gửi</button>
					<button id="btnClose" type="button" data-dismiss="modal" onclick="resetForm()">Đóng</button>
				</div>
			</div>
		</div>
</div>
<?php require_once('../Layouts/footer.php'); 
    }else{
        header('location:../index.php');
      } ?>