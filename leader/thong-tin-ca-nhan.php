<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');;
?>
    <title>Thông tin cá nhân</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    <section class="home-section">
        <?php require_once('./headbar.php') ?>
        <div class="dashboard">
            <div class="welcome-staff row">
				<div class="welcome-title col-md-6">
					<h2>Thông tin cá nhân</h2>
				</div>
			</div>
            </header>
            <br>
            <div class="info-person m-auto">
                    <form role="form" id="info-person" class="m-auto">
							<img id="user-avatar-per" src="../images/<?php echo $_SESSION['avatar'];?>" alt="user-avatar">
							<div class="row mx-5">
								<div class="col py-2">
									<label class="control-label" for="name">Họ và tên:</label>
									<input type="text" class="form-control" name="name" id="name1" value = "<?php echo $_SESSION['name'];?>" disabled>
								</div>
								<div class="col"></div>
							</div>
						<div class="row mx-5">
							<div class="col py-2">
								<label class="control-label" for="name">Tên người dùng:</label>
								<input type="text" class="form-control" name="username" id="username1" value = "<?php echo $_SESSION['username'];?>" disabled>
							</div>
						</div>
						<div class="row mx-5">
							<div class="col py-2">
								<label class="control-label" for="department">Phòng ban:</label>
								<input type="text" class="form-control" name="department" id="department1" value = "<?php echo $_SESSION['department'];?>" disabled>
							</div>
							<div class="col py-2">
								<label class="control-label" for="user-name">Chức vụ:</label>
								<input type="text" class="form-control" name="role" id="role1" value = "<?php echo $_SESSION['role'];?>">
							</div>
						</div>
						<div class="row mx-5">
							<div class="col py-2">
								<label class="control-label" for="day-leave">Số ngày đã xin nghỉ:</label>
								<input type="number" class="form-control" name="day-leave" id="day-leave1" value = "<?php echo $_SESSION['day_leave'];?>">
							</div>
							<div class="col"></div>
						</div>
					</form>
            </div>
        </div>
    </section>
<?php require_once('../Layouts/footer.php'); 
}else{
	header('location:../index.php');
  }?>