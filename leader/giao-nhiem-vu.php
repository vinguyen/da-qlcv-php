<?php
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');
?>
    <title>Giao nhiệm vụ</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>

<section class="home-section">
    <?php require_once('./headbar.php') ?>
    <div class="dashboard">
        <div class="welcome">
                <h1>Xin chào <span class="user-name"> tên người dùng!</span></h1>
        </div>
        <div class="task-board py-2">
            <h3 class="py-2">Danh sách nhân viên trong phòng ban</h3>
            <table class="table table-bordered task-table">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ và tên</th>
                        <th>Tên người dùng</th>
                        <th>Chức vụ</th>
                        <th>Thao tác</th>
                    </tr>
                </thead>
                <tbody id="department-staff">
                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>nguyenvana</td>
                        <td>Nhân viên</td>
                        <td><button class="creatTaskInfo"  data-toggle="modal" data-target="#myAddTask">Giao nhiệm vụ</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="myAddTask">
		<div class="modal-dialog">
			<div class="modal-content py-3">
				<header class="head-form">
					<h3 id="header-title" class="px-3">Tạo nhiệm vụ</h3>
				</header>
				<!-- Modal body -->
				<div class="modal-body">
					<form role="form" id="show-Info-Department" class="mx-3">
						<div class="row">
							<div class="form-group">
								<label class="control-label" for="creat-task">Tiêu đề:</label>
								<input type="text" class="form-control" name="creat-task" id="creat-task" value = "">
                            </div>
						</div>
                        <label class="control-label" for="task-description">Mô tả:</label>
                        <div class="row">
                            <div class="form-group">
                                <textarea name="task-description" class="form-control" id="task-description" cols="40" rows="4" placeholder=""></textarea>
                            </div>
                        </div>
                        <div class="row">
							<div class="form-group">
									<label class="control-label" for="date-task">Thời gian:</label>
									<input type="date" class="form-control" name="date-task" id="date-task" value = "">
                            </div>
						</div>
						<div class="row">
                            <div class="custom-file">
                                <input type="file" class=" form-control custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Tệp đính kềm</label>
                            </div>
                        </div>
					</form>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer" id="modal-footer">
					<button id="btnGiveTask" onclick="sendTask()" type="button">Giao nhiệm vụ</button>
					<button id="btnClose" type="button" data-dismiss="modal" onclick="resetForm()">Đóng</button>
				</div>
			</div>
		</div>
</div>
<?php require_once('../Layouts/footer.php');
    }else{
        header('location:../index.php');
      } ?>
