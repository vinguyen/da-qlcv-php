  <section class="sidebar">
    <div class="logo-sidebar">
        <i class="fab fa-digital-ocean"></i>
    </div>
    <ul class="menu">
      <div class="item admin">
        <a href="trang-chu.php">
          <i class="material-icons title">dashboard</i>
          <span>Trang chủ</span>
        </a>
      </div>
      <!-- Chỗ nãy của trưởng phòng -->
      <div class="item leader">
          <a href="quan-ly-cong-viec.php">
          <i class="fa fa-tasks title"></i>
            <span>Công việc</span>
          </a>
      </div>
      <!-- của nhân viên và trưởng phòng -->
      <div class="item staff">
          <a href="nghi-phep.php">
            <i class="fa fa-check-double title"></i>
            <span>Nghỉ phép</span>
          </a>
      </div>
</section>

