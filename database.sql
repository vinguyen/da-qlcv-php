-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2021 at 01:16 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database`
--

-- --------------------------------------------------------

--
-- Table structure for table `day-leave`
--

CREATE TABLE `day-leave` (
  `id` int(11) NOT NULL,
  `username` int(200) NOT NULL,
  `day` int(11) NOT NULL,
  `process` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `roomNumber` int(11) NOT NULL,
  `leader` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `description`, `roomNumber`, `leader`) VALUES
(1, 'Marketing', 'Xây dựng và phát triển hình ảnh thương hiệu, chiến lược marketing, thiết lập mối quan hệ với truyền thông', 203, '');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `department` varchar(200) NOT NULL,
  `role` varchar(100) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `day_leave` int(11) NOT NULL,
  `department_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `assignee_id` int(3) DEFAULT NULL,
  `created_by` int(3) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `status` tinyint(3) DEFAULT NULL,
  `files` longtext DEFAULT NULL,
  `expired_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

CREATE TABLE `task_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext DEFAULT NULL,
  `files` longtext DEFAULT NULL,
  `created_by` int(3) DEFAULT NULL,
  `created_at` datetime(5) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `task_id` int(3) DEFAULT NULL,
  `type` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `files` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(3) DEFAULT NULL,
  `type` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `employee` admin12345
--


INSERT INTO `employee` (`id`, `name`, `username`, `password`, `department`, `role`, `avatar`, `day_leave`) VALUES
(1, 'Quản trị viên', 'admin', '$2y$10$SQ7crGNofKKsjqtPx/UNEOR67U/Tt2/JDWozMr4fji/5FkprNiyN6', 'Quản trị', 'Quản trị viên', 'NO-IMAGE-AVAILABLE.jpg', 0),
(2, 'Nguyễn Thị Hương', 'nguyenthihuong', '$2y$10$LdQVP3.jAVkJW5bulpL9WOyjfQsZ3INvsbNT1aIkgbfj6PdU31b.W', 'Marketing', 'Trưởng phòng', 'NO-IMAGE-AVAILABLE.jpg\r\n', 0),
(3, 'Nguyễn Văn Bình', 'vanbinh123', '$2y$10$t.shh7tjH2mco3LiPxZCIeeJaW2R/rr3RXGCB8a961I7JoeX3p.Yi', 'Marketing', 'Nhân viên', 'NO-IMAGE-AVAILABLE.jpg', 0),
(4, 'Ngô Diễm Huê', 'ngodiemhue', '$2y$10$HH7CESV2iD/k6EmaI8LSzeDeembh1hjPwlF7Gfcm79rnKFrDXuKsi', 'Marketing', 'Nhân viên', 'NO-IMAGE-AVAILABLE.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `file` text NOT NULL,
  `username` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `day-leave`
--
ALTER TABLE `day-leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `day-leave`
--
ALTER TABLE `day-leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
