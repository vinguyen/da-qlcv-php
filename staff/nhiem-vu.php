<?php 
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        require_once('../Layouts/header.php');
?>
    <title>Nhiệm vụ</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    
<section class="home-section">
    <?php require_once('./headbar.php') ?>
    <div class="dashboard">
        <div class="task-body">
        <header>
            <h2>Tiêu đề nhiệm vụ</h2>
            
        </header>
            <div class="task-description">
                Mô thả chi tiết của task
            </div>
            <div class="task-file">
                tệp đính kèm
            </div>
            <div class="task-deadline">
                deadline ở đây
            </div>
            <div class="task-btn">
                <button id="start-task">Start</button>
            </div>
        </div>
    </div>
</section>
<?php require_once('../Layouts/footer.php'); 
    }else{
        header('location:../index.php');
      } ?>