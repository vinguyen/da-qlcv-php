<?php 
    // echo password_hash('admin12345',PASSWORD_BCRYPT);
    session_start();
    include('../config/db.php');
    if(isset($_SESSION['id'])){
        $error = "";
        require_once('../Layouts/header.php');
        if(isset($_POST['SavePW'])){
            $oldPass = $_POST['oldPW'];
            $newPass = $_POST['newPW'];
            $confirmPW = $_POST['newPW1'];
            if(empty($oldPass)){
                header("Location: doi-mat-khau.php?error=Mật khẩu cũ không được để trống");
                exit();
            }else if(strlen($newPass)<6){
                header("Location: doi-mat-khau.php?error=Mật khẩu phải hơn 6 ký tự");
                exit();
            }else if(empty($newPass)){
                header("Location: doi-mat-khau.php?error=Mật khẩu mới không được để trống");
                exit();
            }else if($newPass != $confirmPW){
                header("Location: doi-mat-khau.php?error=Mật khẩu không đúng");
                exit();
            }else{
                $oldPass = password_hash($oldPass,PASSWORD_BCRYPT);
                $newPass = password_hash($newPass,PASSWORD_BCRYPT);
                $id = $_SESSION['id'];
                $employee = mysqli_query($connect, "SELECT * FROM employee WHERE `id`='$id'");
                $emp = mysqli_fetch_assoc($employee);
                if(mysqli_num_rows($employee)===1 && strcmp($emp['password'],$oldPass))
                    $connect->query("UPDATE `employee` SET `password`='$newPass' WHERE id=$id") or die($connect->error);
                else{
                    header("Location: doi-mat-khau.php?error=Sai mật khẩu");
                exit();
                }
            }
        }
?>
    <title>Thay đổi mật khẩu</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>
    <section class="home-section">
        <?php require_once('./headbar.php') ?>
        <div class="dashboard">
            <div class="welcome-staff row">
				<div class="welcome-title col-md-6">
				</div>
			</div>
            <br>
            <div class="change-form m-auto">
                <form class="change-form1 m-auto py-5" action="" method="POST">
                    <?php
                        if(isset($_GET['error'])){?>
                        <div class="class='alert alert-danger rounded">
                            <p class="p-2"><?php echo $_GET['error'];?></p>
                        </div>
                        <?php } ?>
                    <div class="form-group">
                        <label class="control-label" for="oldPW">Nhập mật khẩu cũ:</label>
                        <input type="password" class="form-control" name="oldPW" id="oldPW" value = "">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="newPW">Nhập mật khẩu mới:</label>
                        <input type="password" class="form-control" name="newPW" id="newPW" value = "">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="newPW1">Nhập lại mật khẩu mới:</label>
                        <input type="password" class="form-control" name="newPW1" id="newPW1" value = "">
                    </div>
                    <div class="text-center">
                        <button type="submit" id="btnSavePW" name="SavePW"></i>Lưu thay đổi</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
<?php require_once('../Layouts/footer.php'); 
}else{
    header('location:../index.php');
  }
?>