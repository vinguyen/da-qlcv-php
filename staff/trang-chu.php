<?php
    session_start();
    include('../config/db.php');
$connect = mysqli_connect('localhost', 'root', '', 'doan');

if(isset($_SESSION['id'])){
$user_id = $_SESSION['id'];
        require_once('../Layouts/header.php');
?>
    <title>Trang chủ</title>
</head>
<body>
    <?php require_once('./sidebar.php') ?>

<section class="home-section">
    <?php require_once('./headbar.php') ?>
    <div class="dashboard">
        <div class="welcome">
                <h1>Danh sách công việc</h1>
        </div>
        <br><br>
        <div class="task-board">
            <table class="table table-bordered task-table">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Tiêu đề</th>
                    <th>Người nhận</th>
                    <th>Thời gian tạo</th>
                    <th>Dealine</th>
                    <th>Trạng thái</th>
                </tr>
                </thead>
                <tbody id="task-list-leader success">
                <?php
                $sql = "SELECT tasks.id, tasks.title, tasks.status, employee.name as assignee_name, 
                        tasks.expired_at,tasks.created_at
                        FROM tasks
                        INNER JOIN employee ON tasks.assignee_id = employee.id
                        WHERE assignee_id=".$user_id." and not status=3
                        ORDER BY tasks.created_at DESC
                ";

                $result = $connect->query($sql);
                $index = 0;
                while($row = $result->fetch_assoc()){
                    $index++;
                    ?>
                    <tr>
                        <td><?=$index?></td>
                        <td>
                            <a href="chi-tiet-cong-viec.php?task_id=<?=$row['id']?>"><?= $row['title'] ?></a>
                        </td>
                        <td> <?= $row['assignee_name'] ?> </td>
                        <td><?= date_format(date_create($row['created_at']),"H:i:s d-m-Y") ?></td>
                        <td><?= date_format(date_create($row['expired_at']),"H:i:s d-m-Y") ?></td>
                        <td> <?php
                            switch ($row['status']) {
                                case 1:
                                    echo 'New';
                                    break;
                                case 2:
                                    echo 'In progress';
                                    break;
                                case 3:
                                    echo 'Canceled';
                                    break;
                                case 4:
                                    echo 'Waiting';
                                    break;
                                case 5:
                                    echo 'Rejected';
                                    break;
                                case 6:
                                    echo 'Completed';
                                    break;
                            }

                            ?> </td>
                    </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
</section>
<?php require_once('../Layouts/footer.php');
    }else{
        header('location:../index.php');
      } ?>
