<?php
session_start();
include('../config/db.php');
$connect = mysqli_connect('localhost', 'root', '', 'doan');

if(isset($_SESSION['id'])){
$user_id = $_SESSION['id'];
require_once('../Layouts/header.php');
date_default_timezone_set('Asia/Ho_Chi_Minh');
$task = null;
?>

<?php

if (isset($_GET['task_id']) && isset($_GET['action']) &&  $_GET['action']==='start') {
    $task_id = $_GET['task_id'];
    $updated_at = date('Y-m-d H:i:s');
    $sqlCancel = "UPDATE tasks 
        SET status = '2', updated_at = '$updated_at'
        WHERE id = '$task_id' and assignee_id = '$user_id'
    ";

    $res = mysqli_query($connect, $sqlCancel);

    if ($res) {
        header('Location:'.'/staff/chi-tiet-cong-viec.php?task_id='.$task_id);
    }
    else {
        header('Location:'.'/staff/chi-tiet-cong-viec.php?task_id='.$task_id);
    }
}

?>

<?php
function upload_files($task_id){
    $uploadTo = "../uploads/";
    $allowFileExt = array('jpg','png','jpeg','gif','pdf','doc','csv','zip','txt');
    $fileName = array_filter($_FILES['file_name']['name']);
    $fileTempName=$_FILES["file_name"]["tmp_name"];
    $tableName= trim('files');

    if (empty($fileName)) {
        $error="Please Select files..";
        return $error;
    }
    else if(empty($tableName)){
        $error="You must declare table name";
        return $error;
    }
    else{
        $error=$storeFilesBasename='';

        foreach($fileName as $index=>$file){
            $fileBasename = basename($fileName[$index]);
            var_dump($fileBasename);
            $filePath = $uploadTo.md5($index.$fileBasename.date_timestamp_get(date_create())).'-'.$fileBasename;
            $fileUrl = '/uploads/'.md5($index.$fileBasename.date_timestamp_get(date_create())).'-'.$fileBasename;
            $fileExt = pathinfo($filePath, PATHINFO_EXTENSION);
            if(in_array($fileExt, $allowFileExt)){
                if(move_uploaded_file($fileTempName[$index],$filePath)){
                    store_files($fileBasename, $fileUrl, $task_id);
                }
                else{
                    $error = 'File Not uploaded ! try again';
                }
            }
            else{
                $error .= $_FILES['file_name']['name'][$index].' - file extensions not allowed<br> ';
            }
        }

    }

    return $error;
}

function store_files($fileBasename, $filePath, $task_id){
    $connect = mysqli_connect('localhost', 'root', '', 'doan');
    if (!empty($fileBasename)) {
        $sqlFile = "INSERT INTO files SET 
            file_name = '$fileBasename',
            url = '$filePath',
            parent_id = '$task_id',
            type = 2
        ";
        $exec = mysqli_query($connect, $sqlFile);
        if($exec){
            echo "files are uploaded successfully";
        }else{
            echo  "Error: " .  $sqlFile . "<br>";
        }
    }
}
?>

<?php

if (isset($_POST['submit'])) {
    $content = $_POST['content'];
    $created_at = date('Y-m-d H:i:s');
    $created_by = $_SESSION['id'];
    $task_id = $_GET['task_id'];
    $type = 1;
    if (!empty($content)) {
        $sql1 = "INSERT INTO task_process SET 
                content = '$content',
                created_at = '$created_at',
                task_id = '$task_id',
                type = '$type',
                created_by = '$created_by'
            ";
        $res2 = mysqli_query($connect, $sql1);
        if ($res2 == true) {
            echo upload_files(mysqli_insert_id($connect));
            $sqlCancel = "UPDATE tasks 
                SET status = '4', updated_at = '$created_at'
                WHERE id = '$task_id' and assignee_id = '$user_id'
            ";
            $res = mysqli_query($connect, $sqlCancel);
            if ($res) {
                echo "<script type='text/javascript'>alert('Submit thành công');</script>";
            }
            header("Refresh:0");
        }
        else {
            echo "<script type='text/javascript'>alert('Submit that bai');</script>";
            header("Refresh:0");
        }
    }
}

?>


<title>Chi tiết công việc</title>
</head>

<body>
<?php require_once('./sidebar.php') ?>

<section class="home-section">
    <?php require_once('./headbar.php') ?>
    <div class="dashboard">
        <div class="welcome">
            <h1>Chi tiết công việc</h1>
        </div>
        <?php

        if (isset($_GET['task_id'])) {
            $task_id = $_GET['task_id'];
            $sql = "SELECT tasks.id, tasks.title, tasks.status,
                    tasks.description, employee.name as assignee_name, tasks.expired_at, tasks.created_at, tasks.updated_at
            FROM tasks 
            INNER JOIN employee 
            ON tasks.assignee_id = employee.id
            WHERE tasks.id='$task_id' and tasks.assignee_id='$user_id'";
            $result = $connect->query($sql);

            if ($result) {
                while($row = $result->fetch_assoc()){
                    $task = $row;
                }
                if (empty($task)) {
                    header('Location:'.'/staff/trang-chu.php');
                }
            } else {
                header('Location:'.'/staff/trang-chu.php');
            }

            ?>

            <div class="task-board py-2">
                <table class="table table-bordered task-table">
                    <tbody>
                    <tr>
                        <th style="width: 200px">Tên công việc</th>
                        <td><?= $task['title'] ?></td>
                    </tr>
                    <tr>
                        <th style="width: 200px">Mô tả</th>
                        <td><?= $task['description'] ?></td>
                    </tr>
                    <tr>
                        <th style="width: 200px">Người thực hiện</th>
                        <td><?= $task['assignee_name'] ?></td>
                    </tr>
                    <tr>
                        <th style="width: 200px">Thời gian tạo</th>
                        <td><?= date_format(date_create($task['created_at']),"H:i:s d-m-Y") ?></td>
                    </tr>
                    <tr>
                        <th style="width: 200px">Thời gian cập nhật</th>
                        <td><?= date_format(date_create($task['updated_at']),"H:i:s d-m-Y") ?></td>
                    </tr>
                    <tr>
                        <th style="width: 200px">Dealine</th>
                        <td><?= date_format(date_create($task['expired_at']),"H:i:s d-m-Y") ?></td>
                    </tr>
                    <tr>
                        <th>Trạng thái</th>
                        <td> <?php
                            switch ($task['status']) {
                                case 1:
                                    echo 'New';
                                    break;
                                case 2:
                                    echo 'In progress';
                                    break;
                                case 3:
                                    echo 'Canceled';
                                    break;
                                case 4:
                                    echo 'Waiting';
                                    break;
                                case 5:
                                    echo 'Rejected';
                                    break;
                                case 6:
                                    echo 'Completed';
                                    break;
                            }

                            ?> </td>
                    </tr>
                    <tr>
                        <th style="width: 200px">Tài liệu liên quan</th>
                        <td>
                            <?php
                            $sql2 = "SELECT * FROM files WHERE parent_id=" . $task['id'] . " AND type=1";
                            $result = $connect->query($sql2);
                            while($row = $result->fetch_assoc()){
                                ?>
                                <p>
                                    <a href="<?= $row['url'] ?>" target="_blank"><?= $row['file_name'] ?></a>
                                </p>

                                <?php
                            }
                            ?>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <?php
            if ($task['status'] == 1) {
                ?>
                <div>
                    <a href="?task_id=<?=$task_id?>&action=start">
                        <button class="btn btn-primary">Bắt đầu</button>
                    </a>
                </div>
                <?php
            }
            ?>

            <?php
            if ($task['status'] == 2 || $task['status'] == 5) {
                ?>
                <div>
                    <button
                        class="btn btn-primary"
                        data-toggle="modal"
                        data-target="#submitTask">Submit</button>
                </div>
                <?php
            }
            ?>

            <br><br>
            <h3>
                Tiến độ thực hiện
            </h3>
            <div class="task-board py-2">
                <table class="table table-bordered task-table table-striped">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Nội dung</th>
                            <th>Tập tin liên quan</th>
                            <th>Người thực hiện</th>
                            <th>Thời gian</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                    $sql = "SELECT task_process.id, task_process.content, task_process.status,
                        employee.name as created_name, task_process.type, task_process.created_at
                        FROM task_process
                        INNER JOIN employee 
                        ON task_process.created_by = employee.id
                        WHERE task_process.task_id='$task_id'
                        ORDER BY task_process.created_at DESC
                    ";
                    $result = $connect->query($sql);
                    $index = 0;
                    while($row = $result->fetch_assoc()){
                        $index++;
                    ?>
                        <tr>
                            <td><?=$index?></td>
                            <td><?=$row['content']?></td>
                            <td>
                                <?php
                                $sql2 = "SELECT * FROM files WHERE parent_id=" . $row['id'] . " AND type=2";
                                $result2 = $connect->query($sql2);
                                while($row1 = $result2->fetch_assoc()){
                                    ?>
                                    <p>
                                        <a href="<?= $row1['url'] ?>" target="_blank"><?= $row1['file_name'] ?></a>
                                    </p>

                                    <?php
                                }
                                ?>
                            </td>
                            <td><?=$row['created_name']?></td>
                            <td><?= date_format(date_create($row['created_at']),"H:i:s d-m-Y") ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>

            <?php

        }
        else {
            header('Location:'.'/leader/quan-ly-cong-viec.php');
        }
        ?>

    </div>
</body>
<div class="modal fade" id="submitTask">
    <div class="modal-dialog">
        <div class="modal-content py-3">
            <header class="head-form">
                <h3 id="header-title" class="px-3">Submit kết quả</h3>
            </header>
            <div class="modal-body">
                <form
                    role="form"
                    method="POST" action=""
                    id="show-Info-Department" class="mx-3"
                    enctype="multipart/form-data"
                >

                    <div class="form-group">
                        <label class="control-label" for="task-description">Nội dung:</label>
                        <textarea
                            name="content"
                            class="form-control"
                            style="width: 100%"
                            rows="4"
                            placeholder=""></textarea>
                    </div>
                    <div class="custom-file">
                        <input type="file"
                               class=" form-control custom-file-input"
                               name="file_name[]" id="files"
                               multiple>
                        <label class="custom-file-label" for="customFile">Tệp đính kèm</label>
                    </div>
                    <div class="modal-footer" id="modal-footer">
                        <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                        <button class="btn btn-secondary" type="reset" data-dismiss="modal">Đóng</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <?php require_once('../Layouts/footer.php');
}else{
    header('location:../index.php');
} ?>
