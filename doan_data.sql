/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : doan

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 08/01/2022 19:57:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for day-leave
-- ----------------------------
DROP TABLE IF EXISTS `day-leave`;
CREATE TABLE `day-leave`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` int(200) NOT NULL,
  `day` int(11) NOT NULL,
  `process` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `roomNumber` int(11) NOT NULL,
  `leader` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, 'Marketing', 'Xây dựng và phát triển hình ảnh thương hiệu, chiến lược marketing, thiết lập mối quan hệ với truyền thông', 203, '');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `department` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `day_leave` int(11) NOT NULL,
  `department_id` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, 'Quản trị viên', 'admin', '$2y$10$SQ7crGNofKKsjqtPx/UNEOR67U/Tt2/JDWozMr4fji/5FkprNiyN6', 'Quản trị', 'Quản trị viên', 'NO-IMAGE-AVAILABLE.jpg', 0, NULL);
INSERT INTO `employee` VALUES (2, 'Nguyễn Thị Hương', 'nguyenthihuong', '$2y$10$LdQVP3.jAVkJW5bulpL9WOyjfQsZ3INvsbNT1aIkgbfj6PdU31b.W', 'Marketing', 'Trưởng phòng', 'NO-IMAGE-AVAILABLE.jpg\r\n', 0, 1);
INSERT INTO `employee` VALUES (3, 'Nguyễn Văn Bình', 'vanbinh123', '$2y$10$t.shh7tjH2mco3LiPxZCIeeJaW2R/rr3RXGCB8a961I7JoeX3p.Yi', 'Marketing', 'Nhân viên', 'NO-IMAGE-AVAILABLE.jpg', 0, 1);
INSERT INTO `employee` VALUES (4, 'Ngô Diễm Huê', 'ngodiemhue', '$2y$10$HH7CESV2iD/k6EmaI8LSzeDeembh1hjPwlF7Gfcm79rnKFrDXuKsi', 'Marketing', 'Nhân viên', 'NO-IMAGE-AVAILABLE.jpg', 0, 1);

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files`  (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(3) NULL DEFAULT NULL,
  `type` tinyint(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of files
-- ----------------------------
INSERT INTO `files` VALUES (19, 'bg-book-item.png', '/uploads/936e7a95cacccb4d0389d779b630bf9c-bg-book-item.png', 45, 1);
INSERT INTO `files` VALUES (20, 'book-item-default.png', '/uploads/5207177565a4730f739e055349d0737f-book-item-default.png', 45, 1);
INSERT INTO `files` VALUES (21, 'icon-loading.gif', '/uploads/55199a3a156b9b16b4caad5e90cd188b-icon-loading.gif', 45, 1);
INSERT INTO `files` VALUES (22, 'pvcb.png', '/uploads/8f1f33127625cae0150cb86f2bfedb49-pvcb.png', 45, 1);
INSERT INTO `files` VALUES (23, 'Vector (2).png', '/uploads/6c7648c64d14b93c4eb565926fd0f918-Vector (2).png', 46, 1);
INSERT INTO `files` VALUES (24, 'DoAnCuoiKi-HK1-2021-2022.pdf', '/uploads/282e2cfc89e7b54943cca39b35f51e72-DoAnCuoiKi-HK1-2021-2022.pdf', 46, 1);
INSERT INTO `files` VALUES (25, 'myshaka.txt', '/uploads/8d1eae72d27fdfa1ee9b05e3407e82f2-myshaka.txt', 47, 1);
INSERT INTO `files` VALUES (26, '26379923.jpg', '/uploads/5ca5a2eaaf7bfb69e5f4985b6e3ba254-26379923.jpg', 48, 1);
INSERT INTO `files` VALUES (27, 'icon-arrow-right-1.png', '/uploads/b7fd810fd416319322ce64af78f07f24-icon-arrow-right-1.png', 48, 1);
INSERT INTO `files` VALUES (28, 'icon-arrow-left-1.png', '/uploads/4f158f17a162bc60effcb0317651ed12-icon-arrow-left-1.png', 48, 1);
INSERT INTO `files` VALUES (29, 'myshaka.txt', '/uploads/21dfce236669bf249818c1d9aff8a231-myshaka.txt', 1, 2);
INSERT INTO `files` VALUES (30, 'Vector (2).png', '/uploads/a2b1974cfa1686bf2b5ea0383029f38d-Vector (2).png', 3, 2);
INSERT INTO `files` VALUES (31, 'Vector (2).png', '/uploads/6fc3dbd9bdb04509996e1a148cfb891f-Vector (2).png', 4, 2);
INSERT INTO `files` VALUES (32, 'banner-web-1 (1).png', '/uploads/72f8de74631037ea3b57fb1bdc5897d3-banner-web-1 (1).png', 7, 2);
INSERT INTO `files` VALUES (33, 'banner-web-1.png', '/uploads/9178d08c7d26fa2add7b45638f8c78c4-banner-web-1.png', 7, 2);
INSERT INTO `files` VALUES (34, 'banner-wap-1 (1).png', '/uploads/1ec1a0f2bdc21da9ac4bc28cd49453dc-banner-wap-1 (1).png', 7, 2);
INSERT INTO `files` VALUES (35, 'Vector (2).png', '/uploads/b00db7e744ea6edc8da0ad3d4aa9485a-Vector (2).png', 12, 2);
INSERT INTO `files` VALUES (36, 'DoAnCuoiKi-HK1-2021-2022.pdf', '/uploads/ac4ca5926dcd815e8527273ac7101a88-DoAnCuoiKi-HK1-2021-2022.pdf', 12, 2);

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project`  (
  `id` int(11) NOT NULL,
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` date NOT NULL,
  `file` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for task_process
-- ----------------------------
DROP TABLE IF EXISTS `task_process`;
CREATE TABLE `task_process`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `files` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_by` int(3) NULL DEFAULT NULL,
  `created_at` datetime(5) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `task_id` int(3) NULL DEFAULT NULL,
  `type` tinyint(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of task_process
-- ----------------------------
INSERT INTO `task_process` VALUES (1, 'DDAA', NULL, 3, '2022-01-08 18:07:29.00000', NULL, 45, 1);
INSERT INTO `task_process` VALUES (2, 'Không tốt', NULL, 2, '2022-01-08 19:02:07.00000', NULL, 45, 2);
INSERT INTO `task_process` VALUES (3, 'Lan 2', NULL, 3, '2022-01-08 19:06:15.00000', NULL, 45, 1);
INSERT INTO `task_process` VALUES (4, 'van khong tot', NULL, 2, '2022-01-08 19:06:33.00000', NULL, 45, 2);
INSERT INTO `task_process` VALUES (5, 'lan 3', NULL, 3, '2022-01-08 19:07:50.00000', NULL, 45, 1);
INSERT INTO `task_process` VALUES (6, 'ok ròi day', NULL, 2, '2022-01-08 19:08:01.00000', NULL, 45, 2);
INSERT INTO `task_process` VALUES (7, 'lanm cuoi nhe', NULL, 3, '2022-01-08 19:08:22.00000', NULL, 45, 1);
INSERT INTO `task_process` VALUES (8, 'OKIE', NULL, 2, '2022-01-08 19:22:26.00000', NULL, 45, 2);
INSERT INTO `task_process` VALUES (9, 'okE BẠN', NULL, 3, '2022-01-08 19:22:37.00000', NULL, 45, 1);
INSERT INTO `task_process` VALUES (10, 'OK', NULL, 2, '2022-01-08 19:37:45.00000', NULL, 45, 2);
INSERT INTO `task_process` VALUES (11, 'ok', NULL, 3, '2022-01-08 19:37:56.00000', NULL, 45, 1);
INSERT INTO `task_process` VALUES (12, 'Nop bao cao', NULL, 3, '2022-01-08 19:47:10.00000', NULL, 48, 1);

-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `assignee_id` int(3) NULL DEFAULT NULL,
  `created_by` int(3) NULL DEFAULT NULL,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `status` tinyint(3) NULL DEFAULT NULL,
  `files` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `expired_at` datetime(6) NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `rate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tasks
-- ----------------------------
INSERT INTO `tasks` VALUES (45, 'CV1', 'CV1', 3, 2, '2022-01-08 16:03:34.000000', 6, NULL, '2022-01-08 16:30:00.000000', '2022-01-08 19:43:38.000000', 'OK');
INSERT INTO `tasks` VALUES (46, 'CV3', 'CV3', 3, 2, '2022-01-08 16:19:13.000000', 3, NULL, '2022-01-08 16:22:00.000000', NULL, NULL);
INSERT INTO `tasks` VALUES (47, 'CV4', 'CV4', 4, 2, '2022-01-08 16:29:44.000000', 1, NULL, '2022-01-08 18:29:00.000000', NULL, NULL);
INSERT INTO `tasks` VALUES (48, 'CV5', 'CV5', 3, 2, '2022-01-09 17:34:24.000000', 6, NULL, '2022-01-09 17:36:00.000000', '2022-01-08 19:49:38.000000', 'Good');

SET FOREIGN_KEY_CHECKS = 1;
