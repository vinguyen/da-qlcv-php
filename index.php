<?php
    session_start();
    //echo password_hash('admin1234',PASSWORD_BCRYPT);
    //echo password_hash('vanbinh123',PASSWORD_BCRYPT);
    $msg = '';
    require_once('./config/db.php');
    if(isset($_POST['username'])){
        $username = $_POST['username'];
        $password = $_POST['password'];

        $username = strip_tags(mysqli_real_escape_string($connect, trim($username)));
        $password = strip_tags(mysqli_real_escape_string($connect, trim($password)));
        $query = "SELECT * FROM `employee` where username='".$username."'";
        $tbl = mysqli_query($connect,$query);
        if(mysqli_num_rows($tbl)>0){
            $row = mysqli_fetch_assoc($tbl);
            $password_hash = $row['password'];
            if(password_verify($password, $password_hash)){
                $_SESSION['id']=$row['id'];
                $_SESSION['name']=$row['name'];
                $_SESSION['username']=$row['username'];
                $_SESSION['password']=$row['password'];
                $_SESSION['department']=$row['department'];
                $_SESSION['department_id']=$row['department_id'];
                $_SESSION['role']=$row['role'];
                $_SESSION['day_leave']=$row['day_leave'];
                $_SESSION['avatar']=$row['avatar'];
                if($row['role']=="Quản trị viên"){
                    header("Location: ./admin/trang-chu.php");
                }
                else if($row['role']=="Nhân viên"){
                    header("Location: ./staff/trang-chu.php");
                }
                else if($row['role']=="Trưởng phòng"){
                    header("Location: ./leader/trang-chu.php");
                }
            }
            else{
                $msg = 'Incorrect Password';
            }
        }
        else{
            $msg = 'Invalid Username';
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- BS4 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="./style.css">
    <title>Đăng nhập</title>
</head>
<body>
    <section class="container_login">
        <div class="login_box d-flex flex-column">
            <div class="logo">
                <i class="fab fa-digital-ocean"></i>
                <h1>Xin chào!</h1>
            </div>
            <div class="form_container w-75 m-auto">
            <?php
                    if(!empty($msg)){
                        echo '<p class="alert alert-info"> '.$msg.'</p>';
                    }
                ?>
                <form method="post" name="signin" action="index.php">
                    <div class="form-group">
                      <label for="username">Tên đăng nhập</label>
                      <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                      <label for="password">Mật khẩu</label>
                      <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="w-50 mx-auto">
                        <button type="submit" class="btn btn-primary w-100" id="btn_login">Đăng nhập</button>
                    </div>
                  </form>
            </div>
        </div>

    </section>
    <script src="./main.js"></script>
    <!-- BS4 -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>
</html>
